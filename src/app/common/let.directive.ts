import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appLet]'
})
export class LetDirective<T = unknown> implements OnInit {
  private value: T = null!;
  @Input() set appLet(v: T) {
    this.value = v;
  };
  // @Input() set appUnless(condition: boolean) {
  //   if (!condition && !this.hasView) {
  //     this.viewContainer.createEmbeddedView(this.templateRef);
  //     this.hasView = true;
  //   } else if (condition && this.hasView) {
  //     this.viewContainer.clear();
  //     this.hasView = false;
  //   }
  // }

  // private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { }

  ngOnInit(): void {
    this.viewContainer.createEmbeddedView(this.templateRef);
  }

}
