import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LetDirective } from './common/let.directive';
import { BooksModule } from './components/books/books.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BooksModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
