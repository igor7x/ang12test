import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface IBook {
  id: number;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books$ = new BehaviorSubject([]);
  currentBook$ = new BehaviorSubject<Partial<IBook>>({});

  constructor() { }

  setCurrentBook() {
    this.currentBook$.next({
      id: 1,
      title: 'my book 1'
    })
  }
}
