import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../../app.module';
import { LetDirModule } from '../../common/let-dir.module';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book-list/book-list.component';



@NgModule({
  declarations: [
    BookComponent,
    BookListComponent
  ],
  exports: [
    BookListComponent
  ],
  imports: [
    CommonModule,
    LetDirModule
  ]
})
export class BooksModule { }
